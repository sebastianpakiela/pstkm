package com.example.pstkm.model;

import lombok.Getter;
import lombok.Setter;

public class AnnealingParameters {

    @Getter
    @Setter
    private double temperature;

    @Getter
    @Setter
    private double temperatureDecreaseRate;

    @Getter
    @Setter
    private int numberOfSteps;

    @Getter
    @Setter
    private int numberOfStages;

}
