package com.example.pstkm.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class Demand {

    @Getter
    @Setter
    private Node origin;

    @Getter
    @Setter
    private Node terminus;

    @Getter
    @Setter
    private int amount;

    @Getter
    @Setter
    private List<Path> paths;


    @Override
    public String toString() {
        return "Demand #" + origin.getId() + " - #" + terminus.getId() + "; Amount " + amount;
    }
}
