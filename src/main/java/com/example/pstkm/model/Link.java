package com.example.pstkm.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Link {

    @JsonProperty("origin")
    @Getter
    @Setter
    private Node origin;

    @JsonProperty("terminus")
    @Getter
    @Setter
    private Node terminus;

    @JsonProperty("fibers")
    @Getter
    @Setter
    private List<Fiber> fibers;

    @JsonProperty("weight")
    @Getter
    @Setter
    private double weight;


    public Link(Node origin, Node terminus, double weight) {
        this.origin = origin;
        this.terminus = terminus;
        this.weight = weight;
        this.fibers = new ArrayList<>();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Link) {
            Link object = (Link) obj;

            if (object.terminus.equals(this.terminus) && object.origin.equals(this.origin)) {
                return true;
            }
            return false;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int result = origin.hashCode();
        result = 31 * result + terminus.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Link from #" + origin.getId() + " node to #" + terminus.getId() + " node.";
    }
}