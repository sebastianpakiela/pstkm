package com.example.pstkm.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.apache.log4j.Logger;
import org.jgrapht.ListenableGraph;
import org.jgrapht.graph.DefaultEdge;

import java.io.IOException;
import java.util.*;


/**
 * Graph is mutable
 */
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class NetworkGraph {

    private static final Logger logger = Logger.getLogger(NetworkGraph.class);

    @JsonProperty("nodes")
    @Getter
    @Setter
    private Map<Integer, Node> nodes;

    @JsonProperty("links")
    @Getter
    @Setter
    private Set<Link> links;

    public NetworkGraph() {
        nodes = new HashMap<>();
        links = new TreeSet<>((Comparator<Link>) (o1, o2) -> o1.equals(o2) ? 0 : 1);
    }

    public static NetworkGraph deserializeGraph(String jsonString) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            NetworkGraph graph = mapper.readValue(jsonString, NetworkGraph.class);

            fixConnectionsInGraph(graph);

            return graph;
        } catch (IOException e) {

            logger.error(e);
            return null;
        }
    }

    private static void fixConnectionsInGraph(NetworkGraph graph) {

        ArrayList<Link> links = new ArrayList<>();
        links.addAll(graph.getLinks());

        for (Link link : links) {
            link.getOrigin().addOriginatingLink(link);
            link.getTerminus().addOriginatingLink(link);
        }
    }

    public static NetworkGraph jgraphToNetworkGraph(ListenableGraph graph) {
        NetworkGraph networkGraph = new NetworkGraph();

        for (Object object : graph.vertexSet()) {
            networkGraph.addNode(new Node(new Integer(object.toString())));
        }

        Set<Link> links = networkGraph.getLinks();
        Map<Integer, Node> nodes = networkGraph.getNodes();

        ArrayList<CustomWeightedEdge> edges = new ArrayList<>(graph.edgeSet());
        for (CustomWeightedEdge edge : edges) {
            String edgeSource = (String) graph.getEdgeSource(edge);
            String edgeTarget = (String) graph.getEdgeTarget(edge);

            links.add(new Link(nodes.get(Integer.parseInt(edgeSource)), nodes.get(Integer.parseInt(edgeTarget)), edge.getWeight()));
        }


        return networkGraph;
    }

    public boolean addLink(Node origin, Node terminus, double weight) {
        if (nodes.containsKey(origin.getId()) && nodes.containsKey(terminus.getId())) {

            Link link = new Link(origin, terminus, weight);

            if (!links.contains(link)) {
                links.add(link);
                origin.addOriginatingLink(link);
                terminus.addTerminatingLink(link);


                return true;
            }
            return false;

        } else {
            return false;
        }
    }

    public boolean addNode(Node node) {
        if (nodes.containsKey(node.getId())) {
            return false;
        } else {

            int iterator = nodes.size() + 1;
            node.setId(iterator);
            nodes.put(iterator, node);
            return true;
        }
    }

    public String serializeGraph() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            logger.error(e);
            return null;
        }
    }

    public List<Link> getLinksList() {
        return new ArrayList(links);
    }


}
