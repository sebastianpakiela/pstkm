package com.example.pstkm.model;

public interface ActionFinishedListener<T> {


    void onActionFinished(T result);
}
