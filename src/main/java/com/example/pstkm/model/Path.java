package com.example.pstkm.model;

import com.example.pstkm.exception.NoSuchNodeException;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

public class Path {

    @Getter
    @Setter
    private List<Link> links;

    public Path() {
        links = new ArrayList<>();
    }

    @JsonIgnore
    public Node getPathOrigin() throws NoSuchNodeException {
        if (!CollectionUtils.isEmpty(links)) {
            return links.get(0).getOrigin();
        } else throw new NoSuchNodeException("Path has no origin");
    }

    @JsonIgnore
    public Node getPathTerminus() throws NoSuchNodeException {
        if (!CollectionUtils.isEmpty(links)) {
            return links.get(links.size() - 1).getTerminus();
        } else throw new NoSuchNodeException("Path has no terminus");
    }

    public void addLink(Link link) {
        links.add(link);
    }

    public boolean isLinkInPath(Link link) {
        return links.contains(link);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("    ").append(links.get(0).getOrigin().toString());

        for (int i = 0; i < links.size(); i++) {
            builder.append(" : " + links.get(i).getTerminus());
        }

        return builder.toString();
    }
}
