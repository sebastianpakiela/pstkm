package com.example.pstkm.model;

import java.util.ArrayList;

public class EdgeInterceptor {

    private static final ArrayList<CustomWeightedEdge> edges = new ArrayList<>();

    public static void intercept(CustomWeightedEdge customWeightedEdge) {
        edges.add(customWeightedEdge);
    }

    public static void setEdgeWeight(CustomWeightedEdge givenEdge, double weight) {
        String pattern = givenEdge.toString().substring(0, givenEdge.toString().indexOf(")"));

        for (CustomWeightedEdge edge : edges) {
            if (edge.toString().startsWith(pattern)) {
                edge.setWeight(weight);
            }
        }
    }
}
