package com.example.pstkm.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Project {
    //probably more data will be useful

    @Getter
    @Setter
    @JsonProperty("graph")
    private NetworkGraph graph;

    @Getter
    @Setter
    @JsonProperty("demands")
    private List<Demand> demands;

    @Getter
    @Setter
    @JsonProperty("maxLambdas")
    private int maxLambdas;

    public static Project readProjectFile(String filename) throws IOException {
        File file = new File(filename);

        BufferedReader reader = new BufferedReader(new FileReader(file));

        String buffer;
        StringBuilder stringBuilder = new StringBuilder();

        while ((buffer = reader.readLine()) != null) {
            stringBuilder.append(buffer);
        }

        String jsonString = stringBuilder.toString();

        ObjectMapper mapper = new ObjectMapper();
        Project project = mapper.readValue(jsonString, Project.class);

        reader.close();

        return project;

    }

    public void saveProjectToFile(String fileName) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(new File(fileName), this);
    }
}
