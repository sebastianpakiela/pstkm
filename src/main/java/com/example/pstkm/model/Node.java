package com.example.pstkm.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Node {


    private Set<Link> originatingLinks;

    private Set<Link> terminatingLinks;

    @JsonProperty("id")
    @Getter
    @Setter
    private Integer id;

    public Node() {
        originatingLinks = new HashSet<>();
        terminatingLinks = new HashSet<>();
    }

    public Node(Integer id) {
        originatingLinks = new HashSet<>();
        terminatingLinks = new HashSet<>();
        this.id = id;
    }

    public Node(String id) {
        originatingLinks = new HashSet<>();
        terminatingLinks = new HashSet<>();
        this.id = Integer.parseInt(id);
    }

    public void addOriginatingLink(Link link) {
        this.originatingLinks.add(link);
    }

    public void addTerminatingLink(Link link) {
        this.terminatingLinks.add(link);
    }

    public boolean isLinkTerminatingAtNode(Link link) {
        return this.terminatingLinks.contains(link);
    }

    public boolean isLinkOriginatingAtNode(Link link) {
        return this.terminatingLinks.contains(link);
    }

    @JsonIgnore
    public Set<Link> getOriginatingLinks() {
        return originatingLinks;
    }

    @JsonIgnore
    public void setOriginatingLinks(Set<Link> originatingLinks) {
        this.originatingLinks = originatingLinks;
    }

    @JsonIgnore
    public Set<Link> getTerminatingLinks() {
        return terminatingLinks;
    }

    @JsonIgnore
    public void setTerminatingLinks(Set<Link> terminatingLinks) {
        this.terminatingLinks = terminatingLinks;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Node) {
            Node obj1 = (Node) obj;
            if (this == obj1 || this.getId().equals(obj1.getId())) {
                return true;
            } else {
                return false;
            }
        } else {

            return false;
        }
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        return "#" + id;
    }
}
