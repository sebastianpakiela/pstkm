package com.example.pstkm.model;

import org.jgrapht.graph.DefaultWeightedEdge;

public class CustomWeightedEdge extends DefaultWeightedEdge {
    private double weight;

    public CustomWeightedEdge() {
        super();
        EdgeInterceptor.intercept(this);
    }

    @Override
    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return super.toString() + " waga: " + weight;
    }
}
