package com.example.pstkm.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Fiber {

    @JsonProperty("lambdaAllocation")
    @Getter
    @Setter
    private Set<Integer> lambdaAllocation;

    public Fiber(Set<Integer> lambdaAllocation) {
        this.lambdaAllocation = new HashSet<>(lambdaAllocation);
    }

    public boolean allocateLambda(int lambdaNumber) {
        if (lambdaAllocation.contains(lambdaNumber)) {
            return false;
        } else {
            lambdaAllocation.add(lambdaNumber);
            return true;
        }
    }


}
