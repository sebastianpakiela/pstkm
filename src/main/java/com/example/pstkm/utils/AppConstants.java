package com.example.pstkm.utils;

/**
 * Created by Mielek on 30.03.2016.
 */
public class AppConstants {

    public static final String WINDOW_WIDTH = "MainWindow.dimension.width";
    public static final String WINDOW_HEIGHT = "MainWindow.dimension.height";
    public static final String WINDOW_BACKGROUND_COLOR = "MainWindow.background.color";
    public static final String LINKDIALOG_WIDTH = "LinkDialog.dimension.width";
    public static final String LINKDIALOG_HEIGHT = "LinkDialog.dimension.height";

    public static final String AMPL_CONSOLE_LINE_LIMIT = "Console.line.limit";

    private AppConstants() {
    }
}
