package com.example.pstkm.utils;

import javafx.util.Pair;
import org.apache.log4j.Logger;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class PositionUtils {

    private static final Logger logger = Logger.getLogger(PositionUtils.class);

    private PositionUtils() {

    }

    public static List<Pair<Integer, Integer>> getNodesPositions(Dimension componentSize, int pointsNumber) {
        int xStart = (int) (componentSize.getWidth() / 2);
        int yStart = (int) (componentSize.getHeight() * 42 / 100);

        int radius = (int) (StrictMath.min(componentSize.getHeight(), componentSize.getWidth()) * 0.8 / 2);


        double slice = 2 * Math.PI / pointsNumber;
        ArrayList<Pair<Integer, Integer>> pointLocations = new ArrayList<>(pointsNumber);

        double angle;
        double newX;
        double newY;

        for (int i = 0; i < pointsNumber; i++) {
            angle = slice * i - Math.PI * 2 / 3;

            newX = xStart + radius * Math.cos(angle);
            newY = yStart + radius * Math.sin(angle);

            logger.info("Angle " + angle);
            logger.info((newX - xStart) + " " + (newY - yStart));

            pointLocations.add(new Pair<>((int) newX, (int) newY));
        }

        return pointLocations;
    }
}
