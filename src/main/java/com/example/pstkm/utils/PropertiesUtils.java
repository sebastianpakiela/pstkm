package com.example.pstkm.utils;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;

/**
 * Created by Mielek on 29.03.2016.
 */
public class PropertiesUtils {
    public static final String PROPERTIES_PATH_STRING = "resources/pstkm.properties";
    private static final Logger LOGGER = Logger.getLogger(PropertiesUtils.class);
    private static Path propertiesPath;

    private static Properties applicationProperties = new Properties();
    private static PropertiesUtils instance = new PropertiesUtils();

    private PropertiesUtils() {

        try {
            InputStream resourceAsStream = getClass().getClassLoader().getResourceAsStream(PROPERTIES_PATH_STRING);
            applicationProperties.load(resourceAsStream);

        } catch (IOException e) {
            LOGGER.error(e);
            System.exit(1);
        }
    }

    public static PropertiesUtils getInstance() {
        return instance;
    }

    public static int getIntegerPropertie(String key) {
        return Integer.parseInt(applicationProperties.getProperty(key));
    }

    public static double getDoublePropertie(String key) {
        return Double.parseDouble(applicationProperties.getProperty(key));
    }

    public static void storePropertie(String key, Object value) {
        applicationProperties.put(key, value);
    }

    public static void saveProperies() throws IOException {
        try (OutputStream os = Files.newOutputStream(propertiesPath)) {
            applicationProperties.store(os, null);
        }
    }

    public String getProperty(String key) {
        return applicationProperties.getProperty(key);
    }
}
