package com.example.pstkm.view;

import com.example.pstkm.ampl.AmplController;
import com.example.pstkm.exception.WrongFileException;
import com.example.pstkm.model.*;
import com.example.pstkm.optim.OAnnealing;
import com.example.pstkm.optim.OState;
import com.example.pstkm.utils.PositionUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.util.Pair;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;
import org.jgraph.JGraph;
import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.GraphConstants;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.alg.KShortestPaths;
import org.jgrapht.ext.JGraphModelAdapter;
import org.jgrapht.graph.ListenableDirectedWeightedGraph;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainFrame extends JFrame {

    private static final Color DEFAULT_BG_COLOR = Color.decode("#FAFBFF");
    private static final Dimension DEFAULT_SIZE = new Dimension(1000, 600);

    private static final Logger logger = Logger.getLogger(MainFrame.class);
    private static final Dimension GRAPH_SIZE = new Dimension(800, 400);

    //
    private JGraphModelAdapter currentGraphAdapter;
    private JGraph currentGraphView;
    private ListenableDirectedWeightedGraph currentGraph;
    private JTextArea logConsole;
    private JScrollPane logScrollPane;
    private JScrollPane demandScrollPane;
    private JList demandListView;
    private List<Demand> demandList;
    private int lambdas = 3;
    private int kShortestPaths = 1;

    @Override
    public void frameInit() {
        super.frameInit();

        setTitle("PSTKM THIRD DELIVERY");

        setSize(DEFAULT_SIZE);
        setPreferredSize(DEFAULT_SIZE);

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        setLocationRelativeTo(null);

        getContentPane().setLayout(null);

        this.logConsole = new JTextArea(9, 71);
        this.logConsole.setLineWrap(true);
        this.logConsole.setWrapStyleWord(false);
        logConsole.setEditable(false);
        logScrollPane = new JScrollPane(this.logConsole);
        logScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        logScrollPane.setVerticalScrollBar(new JScrollBar());
        logScrollPane.setSize(logScrollPane.getPreferredSize());
        logScrollPane.setLocation(0, 400);
        getContentPane().add(logScrollPane);

        demandListView = new JList();
        demandList = new ArrayList<>();
        DefaultListModel<String> listModel = new DefaultListModel<>();
        demandListView.setModel(listModel);

        demandListView.setMinimumSize(new Dimension(180, 545));
        demandListView.setBackground(Color.WHITE);
        demandListView.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        this.demandScrollPane = new JScrollPane(demandListView);
        demandScrollPane.setLocation(800, 0);
        demandScrollPane.setBackground(Color.WHITE);
        demandScrollPane.setSize(demandListView.getMinimumSize());

        demandListView.addListSelectionListener(e -> e.getFirstIndex());

        demandListView.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_DELETE) {
                    int selectedIndex = demandListView.getSelectedIndex();
                    listModel.remove(selectedIndex);
                    demandList.remove(selectedIndex);
                    logger.debug("REMOVED " + selectedIndex);
                }
            }
        });


        getContentPane().add(demandScrollPane);


        setupMenuBar();

        logToConsole("Witaj w trzecim etapie projektu PSTKM");
        logToConsole("Wczytaj lub stwórz nowy graf");
    }

    private void showProject(String path) {
        currentGraph = createGraphAndItsView();

        Project project = tryLoadingProject(path);
        if (project != null) {
            setupLinksAndNodesView(project, currentGraph);
            lambdas = project.getMaxLambdas();
        } else {
            showErrorLoadingProjectMessage();
        }
    }

    private void showErrorLoadingProjectMessage() {
        // TODO: 23.03.2016 implement!
    }

    private void setupLinksAndNodesView(Project project, ListenableDirectedWeightedGraph g) {
        NetworkGraph graph = project.getGraph();

        for (Map.Entry<Integer, Node> integerNodeEntry : graph.getNodes().entrySet()) {
            g.addVertex(integerNodeEntry.getValue().getId().toString());
        }

        for (Link link : graph.getLinksList()) {
            CustomWeightedEdge edge = (CustomWeightedEdge) g.addEdge(link.getOrigin().getId().toString(), link.getTerminus().getId().toString());


            g.setEdgeWeight(edge, link.getWeight() == 0 ? 1 : link.getWeight());
            EdgeInterceptor.setEdgeWeight(edge, link.getWeight() == 0 ? 1 : link.getWeight());
        }

        int i = 0;
        List<Pair<Integer, Integer>> nodesPositions = PositionUtils.getNodesPositions(currentGraphView.getSize(), graph.getNodes().size());

        for (Map.Entry<Integer, Node> integerNodeEntry : graph.getNodes().entrySet()) {
            positionVertexAt(integerNodeEntry.getValue().getId().toString(),
                    nodesPositions.get(i).getKey(),
                    nodesPositions.get(i).getValue());
            i++;
        }

        pack();

        if (demandList == null) {
            demandList = new ArrayList<>();
        }
        demandList.addAll(project.getDemands());

        DefaultListModel model = (DefaultListModel) demandListView.getModel();

        project.getDemands().forEach(model::addElement);
    }

    private Project tryLoadingProject(String fileName) {
        Project project = null;

        try {
            project = Project.readProjectFile(fileName);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return project;
    }

    private ListenableDirectedWeightedGraph createGraphAndItsView() {
        ListenableDirectedWeightedGraph graph = new ListenableDirectedWeightedGraph(CustomWeightedEdge.class);

        currentGraphAdapter = new JGraphModelAdapter(graph);
        currentGraphView = new JGraph(currentGraphAdapter);
        adjustDisplaySettings(currentGraphView);

        currentGraphView.setSize(GRAPH_SIZE);

        getContentPane().add(currentGraphView);

        setLocationRelativeTo(null);

        currentGraphView.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_DELETE) {
                    Object cell = currentGraphView.getSelectionCell();

                    currentGraph.removeVertex(cell.toString());

                    String[] split = cell.toString().split(":");
                    currentGraph.removeEdge(split[0].substring(1, 2), split[1].substring(1, 2));

                }
            }
        });

        return graph;
    }


    private void setEditMode(boolean b) {
        getMenuBar().getMenu(2).getItem(0).setEnabled(b);
        getMenuBar().getMenu(2).getItem(1).setEnabled(b);

        for (int i = 0; i < getMenuBar().getMenu(3).getItemCount(); i++) {
            getMenuBar().getMenu(3).getItem(i).setEnabled(b);
        }
    }

    private void adjustDisplaySettings(JGraph jg) {
        jg.setPreferredSize(GRAPH_SIZE);

        Color c = DEFAULT_BG_COLOR;
        jg.setBackground(c);
    }


    private void positionVertexAt(Object vertex, int x, int y) {
        DefaultGraphCell cell = currentGraphAdapter.getVertexCell(vertex);
        Map attr = cell.getAttributes();
        Rectangle2D b = GraphConstants.getBounds(attr);

        GraphConstants.setBounds(attr, new Rectangle((int) (x - b.getWidth() / 2), (int) (y - b.getHeight() / 2), (int) b.getWidth(), (int) b.getHeight()));

        Map cellAttr = new HashMap();
        cellAttr.put(cell, attr);
        currentGraphAdapter.edit(cellAttr, null, null, null);
    }


    private void setupMenuBar() {
        //presenter
        MenuBar menuBar = new MenuBar();
        addFileMenu(menuBar);
        addViewMenu(menuBar);
        addEditMenu(menuBar);
        addPathMenu(menuBar);
        addAmplMenu(menuBar);
        addHeuristicMenu(menuBar);

        setMenuBar(menuBar);

    }

    private void addHeuristicMenu(MenuBar menuBar) {
        Menu menu = new Menu("Obliczenia heurystyczne");
        MenuItem solveItem = new MenuItem("Rozwiąż problem");

        solveItem.addActionListener(e -> {
            AnnealingParametersDialog dialog = new AnnealingParametersDialog();
            dialog.setActionListener(result -> {
                anneal(result);
            });

            dialog.setVisible(true);
        });

        menu.add(solveItem);
        menuBar.add(menu);


    }

    private void anneal(AnnealingParameters params) {
        OAnnealing anneling = new OAnnealing(graphToProject(), params.getTemperature(),
                params.getTemperatureDecreaseRate(), params.getNumberOfStages(), params.getNumberOfSteps());
        anneling.compute();

        OState optimalState = anneling.getOptimalState();
        logConsole.append(optimalState.toString());

    }

    private void addAmplMenu(MenuBar menuBar) {
        Menu amplMenu = new Menu("AMPL");
        MenuItem showAmplWithData = new MenuItem("Pokaż okno AMPLa i eksportuj dane");
        showAmplWithData.addActionListener(e -> {
            try {
                Project projectData = graphToProject();
                JFrame amplFrame = new JFrame("AMPL");
                String amplPath = "amplcml/ampl.exe";
                AmplController cont = new AmplController(Paths.get(amplPath));
                AmplConsole amplConsole = new AmplConsole(cont);
                amplFrame.setContentPane(amplConsole);
                amplConsole.setProjectData(projectData);
                amplFrame.addWindowListener(new WindowListener() {
                    @Override
                    public void windowClosing(WindowEvent e) {
                        cont.stopAmpl();
                    }

                    @Override
                    public void windowOpened(WindowEvent e) {
                    }

                    @Override
                    public void windowClosed(WindowEvent e) {
                    }

                    @Override
                    public void windowIconified(WindowEvent e) {
                    }

                    @Override
                    public void windowDeiconified(WindowEvent e) {
                    }

                    @Override
                    public void windowActivated(WindowEvent e) {
                    }

                    @Override
                    public void windowDeactivated(WindowEvent e) {
                    }
                });
                amplFrame.setVisible(true);
                amplFrame.setMinimumSize(new Dimension(800, 600));
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (WrongFileException e1) {
                e1.printStackTrace();
            }
        });

        MenuItem showAmpl = new MenuItem("Pokaż okno AMPL'a");
        showAmpl.addActionListener(e -> {
            JFrame amplFrame = new JFrame("AMPL");
            String amplPath = "amplcml/ampl.exe";
            try {
                AmplController amplController = new AmplController(Paths.get(amplPath));
                AmplConsole amplConsole = new AmplConsole(amplController);
                amplFrame.setContentPane(amplConsole);
                amplFrame.addWindowListener(new WindowListener() {
                    @Override
                    public void windowClosing(WindowEvent e) {
                        amplController.stopAmpl();
                    }

                    @Override
                    public void windowOpened(WindowEvent e) {
                    }

                    @Override
                    public void windowClosed(WindowEvent e) {
                    }

                    @Override
                    public void windowIconified(WindowEvent e) {
                    }

                    @Override
                    public void windowDeiconified(WindowEvent e) {
                    }

                    @Override
                    public void windowActivated(WindowEvent e) {
                    }

                    @Override
                    public void windowDeactivated(WindowEvent e) {
                    }
                });
                amplFrame.setVisible(true);
                amplFrame.setMinimumSize(new Dimension(800, 600));
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (WrongFileException e1) {
                e1.printStackTrace();
            }
        });

        amplMenu.add(showAmplWithData);
        amplMenu.add(showAmpl);
        menuBar.add(amplMenu);
    }

    private Project graphToProject() {
        Project projectData = new Project();
        projectData.setMaxLambdas(lambdas);
        projectData.setGraph(NetworkGraph.jgraphToNetworkGraph(currentGraph));
        projectData.setDemands(demandList);

        for (Demand demand : demandList) {
            KShortestPaths ksp = new KShortestPaths(currentGraph, demand.getOrigin().getId().toString(), kShortestPaths);

            List<GraphPath> paths = ksp.getPaths(demand.getTerminus().getId().toString());
            List<Path> modelPaths = new ArrayList<>();

            for (int i = 0; i < paths.size(); i++) {
                GraphPath path = paths.get(i);
                Path tempPathModel = new Path();
                for (Object edge : path.getEdgeList()) {
                    Link tempLink = new Link(new Node(currentGraph.getEdgeSource(edge).toString()),
                            new Node(currentGraph.getEdgeTarget(edge).toString()), currentGraph.getEdgeWeight(edge));
                    tempPathModel.addLink(tempLink);
                }
                modelPaths.add(tempPathModel);
            }
            demand.setPaths(modelPaths);
        }

        return projectData;
    }

    private void addViewMenu(MenuBar menuBar) {
        Menu viewMenu = new Menu("Widok");
        MenuItem clearItem = new MenuItem("Wyczyść");
        clearItem.addActionListener(e -> {
            if (MainFrame.this.currentGraphView != null) {
                remove(currentGraphView);
                currentGraphView = null;
                repaint();
                currentGraph = null;
            }
        });

        viewMenu.add(clearItem);
        menuBar.add(viewMenu);
    }

    private void addFileMenu(MenuBar menuBar) {
        Menu fileMenu = new Menu("Plik");
        MenuItem loadItem = new MenuItem("Otwórz plik z grafem");
        loadItem.addActionListener(e -> {

            JFileChooser chooser = new JFileChooser();
            chooser.setVisible(true);
            int result = chooser.showOpenDialog(null);

            if (result == JFileChooser.APPROVE_OPTION) {
                File selectedFile = chooser.getSelectedFile();
                showProject(selectedFile.getAbsolutePath());
            }

            setEditMode(true);
        });

        menuBar.add(fileMenu);


        MenuItem newGraphItem = new MenuItem("Nowa sieć");
        newGraphItem.addActionListener(e -> {
            currentGraph = createGraphAndItsView();
            currentGraph.addVertex("1");
            currentGraph.addVertex("2");

            setEditMode(true);

        });

        MenuItem saveItem = new MenuItem("Zapisz do pliku");
        saveItem.addActionListener(e1 -> {
            JFileChooser chooser = new JFileChooser();
            chooser.setVisible(true);

            int result = chooser.showSaveDialog(null);
            if (result == JFileChooser.APPROVE_OPTION) {
                File selectedFile = chooser.getSelectedFile();

                Project project = graphToProject();

                ObjectMapper mapper = new ObjectMapper();
                try {
                    mapper.writeValue(selectedFile, project);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        fileMenu.add(loadItem);
        fileMenu.add(newGraphItem);
        fileMenu.add(saveItem);
    }

    public void addEditMenu(MenuBar menuBar) {
        Menu editMenu = new Menu("Edycja");
        MenuItem addVertexItem = new MenuItem("Dodaj węzły");

        addVertexItem.addActionListener(e -> {

            int value = -1;

            while (value < 0) {
                String valueRead = JOptionPane.showInputDialog(MainFrame.this, "Ile węzłów dodać?", "", JOptionPane.INFORMATION_MESSAGE);
                if (NumberUtils.isNumber(valueRead)) {
                    value = Integer.parseInt(valueRead);
                }
            }

            int vertexNumber = currentGraph.vertexSet().size();

            for (int i = 1; i <= value; i++) {
                MainFrame.this.currentGraph.addVertex(String.valueOf(vertexNumber + i));
            }
        });
        editMenu.add(addVertexItem);

        MenuItem addEdgeItem = new MenuItem("Dodaj łącze");
        addEdgeItem.addActionListener(e -> {
            LinkDialog dialog = new LinkDialog(true);

            dialog.setOnSelectedListener(new LinksSelectionListener() {
                @Override
                public void onFinish(String id1, String id2, Double weight) {
                    CustomWeightedEdge edge = (CustomWeightedEdge) currentGraph.addEdge(id1, id2);

                    EdgeInterceptor.setEdgeWeight(edge, weight);
                }

                @Override
                public boolean onCheck(String id) {
                    return currentGraph.vertexSet().contains(id);
                }
            });


            dialog.setVisible(true);
        });

        editMenu.add(addEdgeItem);

        addEdgeItem.setEnabled(false);
        addVertexItem.setEnabled(false);

        MenuItem addDemand = new MenuItem("Dodaj zapotrzebowanie");
        addDemand.addActionListener(e -> {
            LinkDialog dialog = new LinkDialog(true);
            dialog.setOnSelectedListener(new LinksSelectionListener() {
                @Override
                public void onFinish(String id1, String id2, Double weight) {
                    Demand demand = new Demand();
                    demand.setAmount(weight.intValue());
                    demand.setOrigin(new Node(Integer.parseInt(id1)));
                    demand.setTerminus(new Node(Integer.parseInt(id2)));
                    DefaultListModel model = (DefaultListModel) demandListView.getModel();
                    model.addElement(demand);
                    demandList.add(demand);
                    logger.debug(demandList.size());
                    logger.debug(demandListView.getModel().getSize());
                }

                @Override
                public boolean onCheck(String id) {
                    return currentGraph.vertexSet().contains(id);
                }
            });
            dialog.setVisible(true);
        });

        editMenu.add(addDemand);

        MenuItem setNbOfLambdas = new MenuItem("Ustaw ilosc lambd");
        setNbOfLambdas.addActionListener(e -> {
            String str = JOptionPane.showInputDialog(MainFrame.this, "Wprowadz ilosc lambd", lambdas);
            if (str != null && !str.isEmpty()) {
                try {
                    int tmp = Integer.parseInt(str);
                    if (tmp > 0) {
                        lambdas = tmp;
                    }
                } catch (NumberFormatException ex) {
                }
            }
        });
        editMenu.add(setNbOfLambdas);

        MenuItem setNbOfKShortestPaths = new MenuItem("Ustaw ilosc sciezek");
        setNbOfKShortestPaths.addActionListener(e -> {
            String str = JOptionPane.showInputDialog(MainFrame.this, "Wprowadz ilosc sciezek", kShortestPaths);
            if (str != null && !str.isEmpty()) {
                try {
                    int tmp = Integer.parseInt(str);
                    if (tmp > 0) {
                        kShortestPaths = tmp;
                    }
                } catch (NumberFormatException ex) {
                }
            }
        });
        editMenu.add(setNbOfKShortestPaths);

        menuBar.add(editMenu);
    }

    public void addPathMenu(MenuBar menuBar) {
        Menu viewMenu = new Menu("Ścieżki");
        MenuItem kShortestItem = new MenuItem("Wyznacz kShortestPaths");
        kShortestItem.addActionListener(e -> {
            LinkDialog dialog = new LinkDialog(false);
            dialog.setOnSelectedListener(new LinksSelectionListener() {
                @Override
                public void onFinish(String id1, String id2, Double weight) {
                    //yolo, niech szuka wszystkich ;P

                    KShortestPaths ksp = new KShortestPaths(currentGraph, id1, kShortestPaths);

                    //tutaj jest całe szukanie ścieżek przez kshortestpath
                    logToConsole("Najkrótsze ścieżki z węzła " + id1 + " do węzła " + id2);

                    List<GraphPath> paths = ksp.getPaths(id2);
                    for (GraphPath path : paths) {
                        logToConsole(path.toString() + " Łączny koszt ścieżki: " + path.getWeight());
                    }

                }

                @Override
                public boolean onCheck(String id1) {
                    return currentGraph.vertexSet().contains(id1);
                }
            });

            dialog.setVisible(true);
        });

        viewMenu.add(kShortestItem);

        MenuItem dijkstraItem = new MenuItem("Wyznacz najkrótszą ścieżkę (Dijkstra)");
        dijkstraItem.addActionListener(e -> {
            LinkDialog dialog = new LinkDialog(false);
            dialog.setOnSelectedListener(new LinksSelectionListener() {
                @Override
                public void onFinish(String id1, String id2, Double text) {
                    List<CustomWeightedEdge> pathBetween = DijkstraShortestPath.findPathBetween(currentGraph, id1, id2);

                    double weight = 0;

                    logToConsole("Najkrótsza ścieżka od " + id1 + " do " + id2 + ":");
                    for (CustomWeightedEdge edge : pathBetween) {
                        logToConsole(edge.toString());
                        weight += edge.getWeight();
                    }

                    logToConsole("Łączny koszt ścieżki: " + weight);
                }

                @Override
                public boolean onCheck(String id1) {
                    return currentGraph.vertexSet().contains(id1);
                }
            });

            dialog.setVisible(true);
        });

        viewMenu.add(dijkstraItem);
        menuBar.add(viewMenu);

    }

    private void logToConsole(String text) {
        logConsole.append(text);
        logConsole.append(System.lineSeparator());

        SwingUtilities.invokeLater(() -> {

            JScrollBar verticalScrollBar = logScrollPane.getVerticalScrollBar();
            verticalScrollBar.setValue(verticalScrollBar.getMaximum());
        });
    }
}