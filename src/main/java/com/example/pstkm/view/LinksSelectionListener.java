package com.example.pstkm.view;

public interface LinksSelectionListener {

    void onFinish(String id1, String id2, Double weight);

    boolean onCheck(String id1);
}
