package com.example.pstkm.view;

import com.example.pstkm.model.ActionFinishedListener;
import com.example.pstkm.model.AnnealingParameters;
import org.apache.commons.lang3.math.NumberUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class AnnealingParametersDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField numberOfStepsTextField;
    private JTextField numberOfStagesTextField;
    private JTextField temperatureTextField;
    private JTextField tempDecreaseRateTextField;
    private JLabel numberStepsError;
    private JLabel numberStagesError;
    private JLabel tempError;
    private JLabel tempRateError;
    private ActionFinishedListener<AnnealingParameters> listener;

    public AnnealingParametersDialog() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(e -> onOK());

        buttonCancel.addActionListener(e -> onCancel());

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(e -> onCancel(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
                JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);


        setSize(new Dimension(600, 300));

        setLocationRelativeTo(getParent());
    }

    public static void main(String[] args) {
        AnnealingParametersDialog dialog = new AnnealingParametersDialog();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }

    private void onOK() {
        dispose();
        if (areFieldsCorrect()) {
            listener.onActionFinished(collectDataFromFields());
        }
    }

    private AnnealingParameters collectDataFromFields() {
        AnnealingParameters parameters = new AnnealingParameters();
        parameters.setNumberOfStages(Integer.parseInt(numberOfStagesTextField.getText()));
        parameters.setNumberOfSteps(Integer.parseInt(numberOfStepsTextField.getText()));
        parameters.setTemperature(Double.parseDouble(temperatureTextField.getText()));
        parameters.setTemperatureDecreaseRate(Double.parseDouble(tempDecreaseRateTextField.getText()));

        return parameters;
    }

    private boolean areFieldsCorrect() {
        boolean flag = true;

        if (NumberUtils.isNumber(tempDecreaseRateTextField.getText())) {
            tempRateError.setText("");
        } else {
            tempRateError.setText("Decrease rate is invalid");
            flag = false;
        }

        if (NumberUtils.isNumber(temperatureTextField.getText())) {
            tempError.setText("");
        } else {
            tempError.setText("Temperature is invalid");
            flag = false;
        }

        if (isInteger(numberOfStagesTextField.getText())) {
            numberStagesError.setText("");
        } else {
            numberStagesError.setText("Number of stages is invalid");
            flag = false;
        }

        if (isInteger(numberOfStepsTextField.getText())) {
            numberStepsError.setText("");
        } else {
            numberStepsError.setText("Number of stages is invalid");
            flag = false;
        }

        return flag;
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public void setActionListener(ActionFinishedListener<AnnealingParameters> annealingParametersActionFinishedListener) {
        this.listener = annealingParametersActionFinishedListener;
    }

    private boolean isInteger(String nb) {
        Double number = Double.parseDouble(nb);
        if (NumberUtils.isNumber(nb) && Math.ceil(number) == number) {
            return true;
        }
        return false;
    }
}
