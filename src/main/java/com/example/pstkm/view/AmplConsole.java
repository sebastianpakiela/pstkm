package com.example.pstkm.view;

import com.example.pstkm.ampl.AmplController;
import com.example.pstkm.ampl.AmplOutputEvent;
import com.example.pstkm.ampl.parser.DataParser;
import com.example.pstkm.exception.WrongFileException;
import com.example.pstkm.model.Project;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by Mielek on 07.04.2016.
 */
public class AmplConsole extends JPanel {
    private JTextField input;
    private JTextArea output;
    private JPanel contentPanel;
    private JMenuBar menuBar;
    private JMenu actionMenu;
    private JMenuItem loadModel;
    private JMenuItem loadData;
    private JMenuItem solve;
    private JMenuItem reset;
    private JMenu parserMenu;
    private JMenuItem parseProjectData;
    private JMenuItem parseProjectAndLoad;
    private AmplController controller;
    private DataParser parser;

    @Getter
    private Project projectData;

    public AmplConsole(AmplController controller) {
        super(new BorderLayout());
        this.controller = controller;
        this.add(contentPanel);
        controller.addOutputListener((AmplOutputEvent e) -> {
            output.append(e.getMessage() + System.lineSeparator());
        });
        controller.addErrorListener((AmplOutputEvent e) -> {
            output.append(e.getMessage() + System.lineSeparator());
        });
        input.addActionListener(a -> {
            if (!StringUtils.isBlank(input.getText())) {
                output.append(input.getText() + System.lineSeparator());
                controller.inputCustomCommand(input.getText());
                input.setText("");
            }
        });
    }

    public static void main(String[] args) throws IOException, WrongFileException {
        AmplController cont = new AmplController(Paths.get("amplcml/ampl.exe"));
        JFrame frame = new JFrame();
        frame.setContentPane(new AmplConsole(cont));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public void setProjectData(Project projectData) {
        this.projectData = projectData;
        if (projectData != null) {
            parser = new DataParser(projectData);
        } else {
            parser = null;
        }
    }

    private void createUIComponents() {
        menuBar = new JMenuBar();
        actionMenu = new JMenu("Action");
        loadData = new JMenuItem("Load Data");
        loadData.addActionListener(e -> {
            Path p = showChooseFileDialog();
            if (p != null) {
                controller.addData(p);
                output.append(String.format("Data from %s added to Ampl", p.toAbsolutePath().toString()));
            }
        });
        loadModel = new JMenuItem("Load Model");
        loadModel.addActionListener(e -> {
            Path p = showChooseFileDialog();
            if (p != null) {
                controller.addModel(p);
                output.append(String.format("Model from %s added to Ampl\n", p.toAbsolutePath().toString()));
            }
        });
        solve = new JMenuItem("Solve");
        solve.addActionListener(e -> {
            controller.solve();
            output.append("Executing solve\n");
        });
        reset = new JMenuItem("Reset");
        reset.addActionListener(e -> {
            controller.reset();
            output.append("Executing reset\n");
        });
        actionMenu.add(loadModel);
        actionMenu.add(loadData);
        actionMenu.add(solve);
        actionMenu.add(reset);
        menuBar.add(actionMenu);

        parserMenu = new JMenu("Parse");

        parseProjectData = new JMenuItem("Parse data");
        parseProjectData.addActionListener(e -> {
            if (parser != null) {
                Path p = showSaveFileDialog();
                if (p != null) {
                    if (parser.parse(p)) {
                        output.append(String.format("Project data parsed to %s path\n", p.toAbsolutePath().toString()));
                    } else {
                        output.append("Error in parsing occurred\n");
                    }
                }
            }
        });

        parseProjectAndLoad = new JMenuItem("Parse data and load");
        parseProjectAndLoad.addActionListener(e -> {
            if (parser != null) {
                Path p = showSaveFileDialog();
                if (p != null) {
                    if (parser.parse(p)) {
                        output.append(String.format("Project data parsed to %s path\n", p.toAbsolutePath().toString()));
                        controller.addData(p);
                        output.append(String.format("Model from %s added to Ampl\n", p.toAbsolutePath().toString()));
                    } else {
                        output.append("Error in parsing occurred\n");
                    }
                }
            }
        });

        parserMenu.add(parseProjectData);
        parserMenu.add(parseProjectAndLoad);
        menuBar.add(parserMenu);
    }

    private Path showChooseFileDialog() {
        final JFileChooser fc = new JFileChooser();
        int retVal = fc.showOpenDialog(this);
        if (retVal == JFileChooser.APPROVE_OPTION) {
            return fc.getSelectedFile().toPath();
        } else {
            return null;
        }
    }

    private Path showSaveFileDialog() {
        final JFileChooser fc = new JFileChooser();
        int retVal = fc.showSaveDialog(this);
        if (retVal == JFileChooser.APPROVE_OPTION) {
            return fc.getSelectedFile().toPath();
        } else {
            return null;
        }
    }
}
