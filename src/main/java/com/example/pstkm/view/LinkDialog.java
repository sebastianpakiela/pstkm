package com.example.pstkm.view;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class LinkDialog extends JDialog {
    private static final Logger logger = Logger.getLogger(LinkDialog.class);

    private static final Dimension DEFAULT_SIZE = new Dimension(500, 180);
    private final boolean weightMode;
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField originTextField;
    private JTextField terminusTextField;
    private JLabel originLabel;
    private JLabel terminusLabel;
    private JLabel originErrorLabel;
    private JLabel terminusErrorLabel;
    private JTextField weightTextField;
    private JLabel weightErrorLabel;
    private JLabel weightLabel;
    private transient LinksSelectionListener listener;

    public LinkDialog(boolean weightVisible) {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(e -> onOK());

        originErrorLabel.setText("");
        terminusErrorLabel.setText("");

        buttonCancel.addActionListener(e -> onCancel());

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        contentPane.registerKeyboardAction(e -> onCancel(
        ), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);


        setMinimumSize(DEFAULT_SIZE);
        setLocationRelativeTo(getOwner());


        //weight controls setup
        if (weightVisible) {
            weightErrorLabel.setText("");
        } else {
            weightErrorLabel.setVisible(false);
            weightTextField.setVisible(false);
            weightLabel.setVisible(false);
        }

        this.weightMode = weightVisible;

    }

    private boolean checkNode(String label, JLabel errorLabel) {
        if (!listener.onCheck(label)) {
            errorLabel.setText("Podany węzeł nie istnieje lub nie należy do grafu");
            return false;
        } else {
            errorLabel.setText("");
            return true;
        }
    }

    private void onOK() {

        String originId = originTextField.getText();
        String terminusId = terminusTextField.getText();

        if (checkNode(originId, originErrorLabel) && checkNode(terminusId, terminusErrorLabel)
                && (!weightMode || (weightMode && checkWeight()))) {

            String weight = weightTextField.getText();

            listener.onFinish(originId, terminusId, StringUtils.isBlank(weight) ? null : Double.parseDouble(weight));
        }
    }

    private boolean checkWeight() {
        if (NumberUtils.isNumber(weightTextField.getText())) {
            return true;
        } else {
            weightErrorLabel.setText("Podana waga jest błędna");
            return false;
        }

    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public void setOnSelectedListener(LinksSelectionListener linksSelectedListener) {
        this.listener = linksSelectedListener;
    }
}
