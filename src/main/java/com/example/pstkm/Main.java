package com.example.pstkm;

import com.example.pstkm.view.MainFrame;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        MainFrame frame = new MainFrame();
        frame.setVisible(true);

    }
}
