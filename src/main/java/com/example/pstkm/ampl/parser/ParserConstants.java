package com.example.pstkm.ampl.parser;

import com.example.pstkm.utils.PropertiesUtils;

/**
 * Created by Mielek on 06.05.2016.
 */
public class ParserConstants {

    public static final String NODE_SET_NAME = PropertiesUtils.getInstance().getProperty("Model.nodeSet.name");
    public static final String LAMBDAS_SET_NAME = PropertiesUtils.getInstance().getProperty("Model.LambdasSet.name");
    public static final String LINK_SET_NAME = PropertiesUtils.getInstance().getProperty("Model.linkSet.name");
    public static final String LINK_FIBER_COST = PropertiesUtils.getInstance().getProperty("Model.linkSet.cost");
    public static final String DEMAND_SET_NAME = PropertiesUtils.getInstance().getProperty("Model.demandSet.name");
    public static final String DEMAND_PATHS_NB = PropertiesUtils.getInstance().getProperty("Model.demandSet.pathsNumber");
    public static final String DEMAND_VALUE = PropertiesUtils.getInstance().getProperty("Model.demandSet.value");
    public static final String DEMAND_PATHS = PropertiesUtils.getInstance().getProperty("Model.demandSet.paths");
    public static final String MODEL_SET = PropertiesUtils.getInstance().getProperty("Model.set");
    public static final String MODEL_PARAM = PropertiesUtils.getInstance().getProperty("Model.param");


    private ParserConstants() {
        super();
    }
}
