package com.example.pstkm.ampl.parser;

import com.example.pstkm.ampl.AmplConstants;
import com.example.pstkm.exception.NoSuchNodeException;
import com.example.pstkm.model.Demand;
import com.example.pstkm.model.Link;
import com.example.pstkm.model.Path;
import com.example.pstkm.model.Project;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Mielek on 06.05.2016.
 */
public class DataParser {
    private static final Logger LOGGER = Logger.getLogger(DataParser.class);
    private static final String DECLARATION = "%s %s :=";
    private Project project;

    public DataParser(Project project) {
        this.project = project;
    }

    public boolean parse(java.nio.file.Path filePath) {
        try (PrintWriter writer = new PrintWriter(Files.newBufferedWriter(filePath))) {
            writeToFile(writer);
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    private void writeToFile(PrintWriter writer) {
        writer.println(AmplConstants.COMMAND_DATA + AmplConstants.END_COMMAND);
        writeNodes(writer);
        writeLambdas(writer);
        writeLinks(writer);
        writeDemands(writer);
    }

    private void writeDemands(PrintWriter writer) {
        StringBuilder demandSetbuilder = new StringBuilder();
        StringBuilder demandValueBuilder = new StringBuilder();
        StringBuilder demandPathsNbBuilder = new StringBuilder();
        StringBuilder demandPathsBuilder = new StringBuilder();
        for (Demand demand : project.getDemands()) {
            demandSetbuilder.append(getDemand(demand));
            demandSetbuilder.append(System.lineSeparator());
            demandValueBuilder.append(getDemandValue(demand));
            demandValueBuilder.append(System.lineSeparator());
            demandPathsNbBuilder.append(getDemandPathsNb(demand));
            demandPathsNbBuilder.append(System.lineSeparator());
            demandPathsBuilder.append(getDemandPaths(demand));
        }

        writer.println(String.format(DECLARATION, ParserConstants.MODEL_SET, ParserConstants.DEMAND_SET_NAME));
        writer.println(demandSetbuilder.toString());
        writer.println(AmplConstants.END_COMMAND);

        writer.println(String.format(DECLARATION, ParserConstants.MODEL_PARAM, ParserConstants.DEMAND_VALUE));
        writer.println(demandValueBuilder.toString());
        writer.println(AmplConstants.END_COMMAND);

        writer.println(String.format(DECLARATION, ParserConstants.MODEL_PARAM, ParserConstants.DEMAND_PATHS_NB));
        writer.println(demandPathsNbBuilder.toString());
        writer.println(AmplConstants.END_COMMAND);

        writer.println(String.format(DECLARATION, ParserConstants.MODEL_PARAM, ParserConstants.DEMAND_PATHS));
        writer.println(demandPathsBuilder.toString());
        writer.println(AmplConstants.END_COMMAND);
    }

    private String getDemandPathsNb(Demand demand) {
        return String.format("\t[%s,%s] %s", demand.getOrigin().getId().toString(), demand.getTerminus().getId().toString(), Integer.toString(demand.getPaths().size()));
    }

    private StringBuilder getDemandPaths(Demand demand) {
        StringBuilder pathsBuild = new StringBuilder();
        int i = 1;
        for (Path path : demand.getPaths()) {
            pathsBuild.append(getDemandPath(path, i++));
        }
        return pathsBuild;
    }

    private StringBuilder getDemandPath(Path path, int pathNb) {
        StringBuilder pathBuilder = new StringBuilder();
        Set<Link> linkSet = new HashSet<>(path.getLinks());
        try {
            int yn = 0;
            for (Link link : project.getGraph().getLinks()) {
                if (linkSet.contains(link)) {
                    yn = 1;
                } else {
                    yn = 0;
                }
                pathBuilder.append(String.format("\t[%s,%s,%s,%s,%s] %s",
                        link.getOrigin().getId().toString(),
                        link.getTerminus().getId().toString(),
                        path.getPathOrigin().getId().toString(),
                        path.getPathTerminus().getId().toString(),
                        Integer.toString(pathNb),
                        Integer.toString(yn)));
                pathBuilder.append(System.lineSeparator());
            }
        } catch (NoSuchNodeException e) {
            LOGGER.error("", e);
        }
        return pathBuilder;
    }

    private String getDemandValue(Demand demand) {
        return String.format("\t[%s,%s] %s", demand.getOrigin().getId().toString(), demand.getTerminus().getId().toString(), Integer.toString(demand.getAmount()));
    }

    private String getDemand(Demand demand) {
        return String.format("\t(%s,%s)", demand.getOrigin().getId().toString(), demand.getTerminus().getId().toString());
    }

    private void writeLinks(PrintWriter writer) {
        StringBuilder linkSetBuilder = new StringBuilder();
        StringBuilder linkCostBuilder = new StringBuilder();
        for (Link link : project.getGraph().getLinks()) {
            linkSetBuilder.append(getLink(link));
            linkSetBuilder.append(System.lineSeparator());
            linkCostBuilder.append(getLinkCost(link));
            linkCostBuilder.append(System.lineSeparator());
        }

        writer.println(String.format(DECLARATION, ParserConstants.MODEL_SET, ParserConstants.LINK_SET_NAME));
        writer.println(linkSetBuilder.toString());
        writer.println(AmplConstants.END_COMMAND);
        writer.println(String.format(DECLARATION, ParserConstants.MODEL_PARAM, ParserConstants.LINK_FIBER_COST));
        writer.println(linkCostBuilder.toString());
        writer.println(AmplConstants.END_COMMAND);
    }

    private String getLinkCost(Link link) {
        return String.format("\t[%s,%s] %s", link.getOrigin().getId().toString(), link.getTerminus().getId().toString(), Double.toString(link.getWeight()));
    }

    private String getLink(Link link) {
        return String.format("\t(%s,%s)", link.getOrigin().getId().toString(), link.getTerminus().getId().toString());
    }

    private void writeLambdas(PrintWriter writer) {
        writer.println(String.format(DECLARATION, ParserConstants.MODEL_SET, ParserConstants.LAMBDAS_SET_NAME));
        for (int i = 1; i <= project.getMaxLambdas(); ++i) {
            writer.println(String.format("\t%s", Integer.toString(i)));
        }
        writer.println(AmplConstants.END_COMMAND);
    }

    private void writeNodes(PrintWriter writer) {
        writer.println(String.format(DECLARATION, ParserConstants.MODEL_SET, ParserConstants.NODE_SET_NAME));
        for (Integer ids : project.getGraph().getNodes().keySet()) {
            writer.println(String.format("\t%s", ids.toString()));
        }
        writer.println(AmplConstants.END_COMMAND);
    }
}
