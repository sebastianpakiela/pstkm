package com.example.pstkm.ampl;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Mielek on 29.03.2016.
 */
class AmplOutputTask implements Runnable {
    private static final Logger LOGGER = Logger.getLogger(AmplOutputTask.class);
    private BufferedReader output;
    private Set<IAmplOutputListener> outputListener = new HashSet<>();
    private boolean running = true;

    public AmplOutputTask(BufferedReader output) {
        this.output = output;
    }

    public void addOutputListener(IAmplOutputListener listener) {
        if (listener != null)
            outputListener.add(listener);
    }

    public void removeOutputListener(IAmplOutputListener listener) {
        outputListener.remove(listener);
    }

    public void stopTask() {
        running = false;
    }

    @Override
    public void run() {
        try (BufferedReader output = this.output) {
            while (running) {
                runRutine(output);
            }
        } catch (IOException e) {
            LOGGER.error("", e);
        }
    }

    private void update(String message) {
        for (IAmplOutputListener lis : outputListener) {
            lis.invoke(new AmplOutputEvent(message));
        }
    }

    private void runRutine(BufferedReader output) throws IOException {
        String message;
        String tmp;
        if ((tmp = output.readLine()) != null) {
            message = tmp;
            update(message);
        } else {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                LOGGER.warn("Ampl output interrupted", e);
                running = false;
                Thread.currentThread().interrupt();
            }
        }
    }
}
