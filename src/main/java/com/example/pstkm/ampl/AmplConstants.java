package com.example.pstkm.ampl;

import com.example.pstkm.utils.PropertiesUtils;

/**
 * Created by Mielek on 02.04.2016.
 */
public class AmplConstants {

    public static final String AMPL_EXE_NAME = PropertiesUtils.getInstance().getProperty("AMPL.fileName");
    public static final String AMPL_LICENCE_NAME = PropertiesUtils.getInstance().getProperty("AMPL.licence");

    public static final String COMMAND_STOP = PropertiesUtils.getInstance().getProperty("AMPL.command.stop");
    public static final String COMMAND_MODEL = PropertiesUtils.getInstance().getProperty("AMPL.command.model");
    public static final String COMMAND_SOLVE = PropertiesUtils.getInstance().getProperty("AMPL.command.solve");
    public static final String COMMAND_RESET = PropertiesUtils.getInstance().getProperty("AMPL.command.reset");
    public static final String COMMAND_DATA = PropertiesUtils.getInstance().getProperty("AMPL.command.data");
    public static final String COMMAND_DISPLAY = PropertiesUtils.getInstance().getProperty("AMPL.command.display");
    public static final String END_COMMAND = PropertiesUtils.getInstance().getProperty("AMPL.endCommand");

    public static final String INIT_COMMANDS = PropertiesUtils.getInstance().getProperty("AMPL.init");


    private AmplConstants() {
    }
}
