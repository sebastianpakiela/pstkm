package com.example.pstkm.ampl;

/**
 * Created by Mielek on 29.03.2016.
 */
public interface IAmplEventListener<event extends AmplEvent> {
    void invoke(event e);
}
