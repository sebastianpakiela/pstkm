package com.example.pstkm.ampl;

import com.example.pstkm.exception.WrongFileException;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.file.Path;

/**
 * Created by Mielek on 29.03.2016.
 */
public class AmplController {
    private static final Logger LOGGER = Logger.getLogger(AmplController.class);

    private Process amplProcess;
    private AmplOutputTask amplOutputTask;
    private AmplOutputTask amplErrorTask;
    private PrintWriter input;

    public AmplController(Path amplPath) throws IOException, WrongFileException {
        if (!amplPath.endsWith(AmplConstants.AMPL_EXE_NAME)) {
            throw new WrongFileException();
        }
        Path directory = amplPath.getParent();
        ProcessBuilder builder = new ProcessBuilder().command(amplPath.toString()).directory(directory.toFile());
        builder.environment().put("Path", builder.environment().get("Path") + ";" + directory.toAbsolutePath().toString());
        amplProcess = builder.start();
        amplOutputTask = new AmplOutputTask(new BufferedReader(new InputStreamReader(amplProcess.getInputStream())));
        amplErrorTask = new AmplOutputTask(new BufferedReader(new InputStreamReader(amplProcess.getErrorStream())));
        new Thread(amplOutputTask).start();
        new Thread(amplErrorTask).start();
        input = new PrintWriter(amplProcess.getOutputStream());
    }

    public boolean isAmplAlive() {
        return amplProcess.isAlive();
    }

    public void reset() {
        inputAmplCommand(AmplConstants.COMMAND_RESET);
    }

    public void init() {
        inputAmplCommand(AmplConstants.INIT_COMMANDS);
    }

    public void addModel(Path modelPath) {
        inputAmplCommand(AmplConstants.COMMAND_MODEL, modelPath.toString());
    }

    public void addData(Path dataPath) {
        inputAmplCommand(AmplConstants.COMMAND_DATA, dataPath.toString());
    }

    public void solve() {
        inputAmplCommand(AmplConstants.COMMAND_SOLVE);
    }

    public void display(String str) {
        inputAmplCommand(AmplConstants.COMMAND_DISPLAY, str);
    }

    private void inputAmplCommand(String command) {
        inputCustomCommand(String.format("%s %s", command, AmplConstants.END_COMMAND));
    }

    private void inputAmplCommand(String command, String value) {
        inputAmplCommand(String.format("%s %s", command, value));
    }

    public void inputCustomCommand(String command) {
        input.println(command);
        input.flush();
    }

    public void stopAmpl() {
        inputAmplCommand(AmplConstants.COMMAND_STOP);
        try {
            if (0 != amplProcess.waitFor()) {
                LOGGER.warn("Ampl ended with error. Exit code:" + amplProcess.exitValue());
            }
        } catch (InterruptedException e) {
            LOGGER.error("", e);
        }
        amplOutputTask.stopTask();
        amplErrorTask.stopTask();
        input.close();
    }

    public void addOutputListener(IAmplOutputListener listener) {
        amplOutputTask.addOutputListener(listener);
    }

    public void addErrorListener(IAmplOutputListener listener) {
        amplErrorTask.addOutputListener(listener);
    }

    public void removeOutputListener(IAmplOutputListener listener) {
        amplOutputTask.removeOutputListener(listener);
    }

    public void removeErrorListener(IAmplOutputListener listener) {
        amplErrorTask.removeOutputListener(listener);
    }
}
