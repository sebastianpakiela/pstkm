package com.example.pstkm.ampl;

import lombok.Getter;

/**
 * Created by Mielek on 29.03.2016.
 */
public class AmplOutputEvent extends AmplEvent {

    @Getter
    private String message;

    AmplOutputEvent(String message) {
        this.message = message;
    }
}
