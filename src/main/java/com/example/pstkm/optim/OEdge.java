package com.example.pstkm.optim;

import com.example.pstkm.model.Link;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.Collections;

/**
 * Created by Mielek on 19.05.2016.
 */
public class OEdge {
    private static final Logger LOGGER = Logger.getLogger(OEdge.class);
    Link link;
    int[] lambdas;

    public OEdge(Link link, int cLambdas) {
        this.link = link;
        lambdas = new int[cLambdas];
    }

    public void reset() {
        for (int i = 0; i < lambdas.length; ++i) {
            lambdas[i] = 0;
        }
    }

    public void addDemand(ODemand d) {
        if (d.getSelectedPath().isLinkInPath(link)) {
            int endIndex = d.getStartLambda() + d.getVolume() >= lambdas.length ? lambdas.length : d.getStartLambda() + d.getVolume();
            for (int i = d.getStartLambda(); i < endIndex; ++i) {
                lambdas[i] += 1;
            }
        }
    }

    public int getNbOfFibers() {
        int max = Collections.max(Arrays.asList(ArrayUtils.toObject(lambdas)));
        LOGGER.debug("Edge " + link.toString() + " has " + max + " fibers");
        return max;
    }

    public double getCurrentEdgeCost() {
        double value = link.getWeight() * getNbOfFibers();
        LOGGER.debug("Edge " + link.toString() + " has " + value + " cost");
        return value;
    }
}
