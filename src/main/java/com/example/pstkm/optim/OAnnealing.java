package com.example.pstkm.optim;

import com.example.pstkm.model.Project;
import org.apache.log4j.Logger;

import java.util.Random;

/**
 * Created by Mielek on 19.05.2016.
 */
public class OAnnealing {
    private static final Logger LOGGER = Logger.getLogger(OAnnealing.class);
    Random rndGenerator;
    double temperature;
    double temperatureDecreaseRate;
    int nbStages;
    int nbSteps;
    int noChangeCounter;
    OState optimalState;
    OState previousState;
    OState currentState;

    public OAnnealing(Project project, double temperature, double temperatureDecreaseRate, int nbStages, int nbSteps) {
        this.temperature = temperature;
        this.temperatureDecreaseRate = temperatureDecreaseRate;
        this.nbStages = nbStages;
        this.nbSteps = nbSteps;
        optimalState = new OState(project.getGraph().getLinksList(), project.getDemands(), project.getMaxLambdas());
        previousState = new OState(project.getGraph().getLinksList(), project.getDemands(), project.getMaxLambdas());
        rndGenerator = new Random();
    }

    public void compute() {
        int currentStage = 0;
        while (evaluateStagesEndingConditions(currentStage++)) {
            int currentStep = 0;
            noChangeCounter = 0;
            while (evaluateStepsEndingConditions(currentStep++)) {
                noChangeCounter++;
                goToNextState();
                LOGGER.debug("Optimal: " + optimalState.getStateCost() + ", previous: " + previousState.getStateCost() + ", current: " + currentState.getStateCost());
                if (nextStateIsBetter() || nexStateIsWorseChoose()) {
                    if (nextStateIsOptimal()) {
                        LOGGER.debug("Found optimal value");
                        optimalState = currentState;
                    }
                    previousState = currentState;
                    noChangeCounter = 0;
                }
            }
            decreaseTemperature();
        }
        LOGGER.debug("Optimal value: " + optimalState.getStateCost());
    }

    public OState getOptimalState() {
        return optimalState;
    }

    private void decreaseTemperature() {
        LOGGER.debug("Decreasing temperature");
        temperature *= temperatureDecreaseRate;
    }

    private boolean nextStateIsOptimal() {
        boolean b = optimalState.getStateCost() > currentState.getStateCost();
        LOGGER.debug("Is next state optimal: " + b);
        return b;
    }

    private boolean nexStateIsWorseChoose() {
        double delta = Math.abs(previousState.getStateCost() - currentState.getStateCost());
        double pstwo = Math.exp(-delta / temperature);
        boolean b = rndGenerator.nextDouble() < pstwo;
        LOGGER.debug("Is worse choose: " + b + " delta: " + delta + " pstwo: " + pstwo);
        return b;
    }

    private boolean nextStateIsBetter() {
        boolean b = previousState.getStateCost() > currentState.getStateCost();
        LOGGER.debug("Is next state better: " + b);
        return b;
    }

    private void goToNextState() {
        LOGGER.debug("Changing state");
        currentState = previousState.getNextState();
    }

    private boolean evaluateStepsEndingConditions(int currentStep) {
        boolean b = currentStep < nbSteps;
        LOGGER.debug("Evaluating step end condition: " + b);
        return b;
    }

    private boolean evaluateStagesEndingConditions(int currentStage) {
        boolean b1 = currentStage < nbStages;
        boolean b2 = noChangeCounter < nbSteps;
        boolean b3 = temperature > 1;
        boolean b = b1 && b2 && b3;
        LOGGER.debug("Evaluating stage end condition: " + b1 + " & " + b2 + " & " + b3 + " " + b);
        return b;
    }
}
