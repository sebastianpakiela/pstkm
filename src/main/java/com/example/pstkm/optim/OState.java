package com.example.pstkm.optim;

import com.example.pstkm.model.Demand;
import com.example.pstkm.model.Link;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Mielek on 19.05.2016.
 */
public class OState {
    private static final Logger LOGGER = Logger.getLogger(OState.class);
    List<ODemand> dList = new ArrayList<>();
    List<OEdge> eList = new ArrayList<>();
    boolean costEvaluated = false;

    OState(List<Link> links, List<Demand> demands, int nbLambdas) {
        for (Link link : links) {
            eList.add(new OEdge(link, nbLambdas));
        }
        for (Demand demand : demands) {
            dList.add(new ODemand(demand, nbLambdas));
        }
    }

    OState(List<OEdge> links, List<ODemand> demands) {
        dList = demands;
        eList = links;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append("OState: " + System.lineSeparator());
        builder.append("Found optimal value: " + getStateCost() + System.lineSeparator());
        builder.append("DemandList" + System.lineSeparator());

        for (ODemand oDemand : dList) {
            builder.append(oDemand.toString() + System.lineSeparator());
        }

        return builder.toString();
    }

    void resetEdges() {
        for (OEdge e : eList) {
            e.reset();
        }
    }

    void evaluateCost() {
        resetEdges();
        for (ODemand d : dList) {
            for (OEdge e : eList) {
                e.addDemand(d);
            }
        }
        costEvaluated = true;
    }

    public double getStateCost() {
        evaluateCost();
        double cost = 0;
        for (OEdge e : eList) {
            cost += e.getCurrentEdgeCost();
        }
        LOGGER.debug("State cost: " + cost);
        return cost;
    }

    public OState getNextState() {
        Random rand = new Random();
        int changeIndex = rand.nextInt(dList.size());
        ODemand od = dList.get(changeIndex);
        LOGGER.debug("Changing demand: " + od.getDemand().toString());
        ODemand d = new ODemand(od);
        d.next();
        List<ODemand> newDemands = new ArrayList<>(dList);
        newDemands.remove(changeIndex);
        newDemands.add(changeIndex, d);
        return new OState(eList, newDemands);
    }

    public List<ODemand> getDemands() {
        return dList;
    }

    public List<OEdge> getEdges() {
        return eList;
    }

    public double getEdgeCost(OEdge oEdge) {
        evaluateCost();
        return oEdge.getCurrentEdgeCost();
    }

    public int getEdgeFibers(OEdge oEdge) {
        evaluateCost();
        return oEdge.getNbOfFibers();
    }
}
