package com.example.pstkm.optim;

import com.example.pstkm.model.Demand;
import com.example.pstkm.model.Path;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Mielek on 19.05.2016.
 */
public class ODemand {
    private static final Logger LOGGER = Logger.getLogger(ODemand.class);
    private Demand demand;
    private int lambdas;
    private int startLambda;
    private Path selectedPath;

    public ODemand(Demand demand, int lambdas) {
        this.demand = demand;
        this.lambdas = lambdas;
        this.startLambda = 0;
        this.selectedPath = demand.getPaths().get(0);
    }

    public ODemand(ODemand o) {
        this.demand = o.demand;
        this.startLambda = o.startLambda;
        this.selectedPath = o.selectedPath;
        this.lambdas = o.lambdas;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("  ").append("Starts at " + demand.getOrigin() + " ends at " + demand.getTerminus()).append(System.lineSeparator());
        builder.append("  ").append("Lambdas:" + lambdas).append(System.lineSeparator());
        builder.append("  ").append("StartLambda:" + startLambda).append(System.lineSeparator());
        builder.append("  ").append("Path: " + selectedPath).append(System.lineSeparator());
        return builder.toString();
    }

    public Path getSelectedPath() {
        return selectedPath;
    }

    public int getStartLambda() {
        return startLambda;
    }

    public void setStartLambda(int startLambda) {
        this.startLambda = startLambda;
    }

    public void next() {
        List<Path> candidates = new ArrayList<>(demand.getPaths());
        candidates.remove(selectedPath);
        Random rand = new Random();
        if (candidates.size() > 0 && lambdas - demand.getAmount() > 0) {
            if (rand.nextDouble() > 0.5) {
                Path sp = candidates.get(rand.nextInt(candidates.size()));
                LOGGER.debug("Changing selected from " + selectedPath.toString() + " to " + sp);
                selectedPath = sp;
            } else {
                int sl = rand.nextInt(lambdas - demand.getAmount());
                if (sl >= startLambda) {
                    sl++;
                }
                LOGGER.debug("Changing start lambda from " + startLambda + " to " + sl + ", demand value: " + demand.getAmount() + " available lambdas: " + lambdas);
                startLambda = sl;
            }
        } else if (candidates.size() > 0) {
            Path sp = candidates.get(rand.nextInt(candidates.size()));
            LOGGER.debug("Changing selected from " + selectedPath.toString() + " to " + sp);
            selectedPath = sp;
        } else if (lambdas - demand.getAmount() > 0) {
            int sl = rand.nextInt(lambdas - demand.getAmount());
            if (sl >= startLambda) {
                sl++;
            }
            LOGGER.debug("Changing start lambda from " + startLambda + " to " + sl + ", demand value: " + demand.getAmount() + " available lambdas: " + lambdas);
            startLambda = sl;
        } else {
            LOGGER.debug("No change made, demand have only 1 path and utilize all available lambdas");
        }
    }

    public Demand getDemand() {
        return demand;
    }

    public int getVolume() {
        return demand.getAmount();
    }
}
