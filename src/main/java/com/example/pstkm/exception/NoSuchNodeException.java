package com.example.pstkm.exception;

public class NoSuchNodeException extends Exception {


    public NoSuchNodeException(String message) {
        super(message);
    }
}
