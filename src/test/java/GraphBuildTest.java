import com.example.pstkm.model.Link;
import com.example.pstkm.model.NetworkGraph;
import com.example.pstkm.model.Node;
import com.example.pstkm.model.Project;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class GraphBuildTest {

    final static Logger logger = Logger.getLogger(GraphBuildTest.class);


    @Test
    public void addNodesTest() {
        NetworkGraph graph = new NetworkGraph();

        assert graph.getLinks() != null;
        assert graph.getNodes() != null;

        addNodes(graph);

        assert graph.getNodes().size() == 5;
    }

    @Test
    public void addLinksTest() {
        NetworkGraph graph = new NetworkGraph();
        addNodes(graph);
        addLinks(graph);

        assert graph.getLinks().size() == 5;
    }

    @Test
    public void graphSerializationDeserializationTest() {

        NetworkGraph graph = buildGraph();

        String jsonString = graph.serializeGraph();
        logger.debug(jsonString);

        assert jsonString != null;

        NetworkGraph deserializeGraph = NetworkGraph.deserializeGraph(jsonString);

        List<Link> linksList = graph.getLinksList();

        Link temp = null;
        for (int i = 0; i < linksList.size(); i++) {
            temp = linksList.get(i);

            Assert.assertTrue("Origin of node #" + i + " is broken", temp.getOrigin() != null && temp.getOrigin().getId() != 0);
            Assert.assertTrue("Terminus of node #" + i + " is broken", temp.getTerminus() != null && temp.getTerminus().getId() != 0);
        }

        logger.info("Test passed");
    }


    @Test
    public void safeProjectTest() {
        Project project = new Project();
        project.setGraph(buildGraph());

        try {
            project.saveProjectToFile("graf.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            project = Project.readProjectFile("graf.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }

        Assert.assertTrue("Read is broken", project != null);
    }


    public void addNodes(NetworkGraph graph) {
        assert graph.getLinks() != null;
        assert graph.getNodes() != null;

        Node n1 = new Node();
        Node n2 = new Node();
        Node n3 = new Node();
        Node n4 = new Node();
        Node n5 = new Node();

        graph.addNode(n1);
        graph.addNode(n2);
        graph.addNode(n3);
        graph.addNode(n4);
        graph.addNode(n5);
        graph.addNode(n5);
        graph.addNode(n5);
        graph.addNode(n5);
        graph.addNode(n5);
    }

    public void addLinks(NetworkGraph graph) {
        Map<Integer, Node> nodes = graph.getNodes();

        graph.addLink(nodes.get(1), nodes.get(1), 1);
        graph.addLink(nodes.get(2), nodes.get(3), 1);
        graph.addLink(nodes.get(3), nodes.get(4), 1);
        graph.addLink(nodes.get(4), nodes.get(5), 1);
        graph.addLink(nodes.get(5), nodes.get(1), 1);
        graph.addLink(nodes.get(5), nodes.get(1), 1);
        graph.addLink(nodes.get(5), nodes.get(1), 1);
        graph.addLink(nodes.get(5), nodes.get(1), 1);
        graph.addLink(nodes.get(5), nodes.get(1), 1);
    }

    public NetworkGraph buildGraph() {
        NetworkGraph graph = new NetworkGraph();

        addNodes(graph);
        addLinks(graph);

        return graph;
    }

}
