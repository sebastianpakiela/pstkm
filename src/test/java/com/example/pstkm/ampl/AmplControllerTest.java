package com.example.pstkm.ampl;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import java.nio.file.Paths;

import static org.junit.Assert.*;

/**
 * Created by Mielek on 02.04.2016.
 */
public class AmplControllerTest {
    private static final Logger LOGGER = Logger.getLogger(AmplControllerTest.class);
    private static final String amplPath = "amplcml/ampl.exe";
    private static final String amplTestModelPath = "testFiles/RM/ampl/diet1.mod";
    private static final String amplTestDataPath = "testFiles/RM/ampl/diet1.dat";

    class LoggerInfo implements IAmplOutputListener {
        @Override
        public void invoke(AmplOutputEvent e) {
            LOGGER.info(e.getMessage());
        }
    }

    class LoggerError implements IAmplOutputListener {
        @Override
        public void invoke(AmplOutputEvent e) {
            LOGGER.error(e.getMessage());
        }
    }

    @BeforeClass
    public static void startUp() {
        LOGGER.debug("test started");
    }

    @Test
    public void startStop_Test() {
        try {
            AmplController cont = new AmplController(Paths.get(amplPath));
            cont.addOutputListener(new LoggerInfo());
            cont.addErrorListener(new LoggerError());
            assertTrue("Ampl not started", cont.isAmplAlive());
            cont.stopAmpl();
            assertFalse("Ampl not stopped", cont.isAmplAlive());
        } catch (Exception e) {
            LOGGER.error("", e);
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void inputOutput_Test() throws Exception {
        AmplController cont = new AmplController(Paths.get(amplPath));
        cont.addOutputListener(new LoggerInfo());
        cont.addErrorListener(new LoggerError());
        cont.init();
        cont.addModel(Paths.get(amplTestModelPath).toAbsolutePath());
        Thread.sleep(1000);
        cont.addData(Paths.get(amplTestDataPath).toAbsolutePath());
        Thread.sleep(1000);
        cont.solve();
        Thread.sleep(1000);
        cont.display("Buy");
        Thread.sleep(1000);
        cont.stopAmpl();
    }
}