package com.example.pstkm.optim;

import com.example.pstkm.model.Demand;
import com.example.pstkm.model.Link;
import com.example.pstkm.model.Node;
import com.example.pstkm.model.Path;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;

/**
 * Created by Mielek on 31.05.2016.
 */
public class ODemandTest {
    @Test
    public void creationTest() {
        Node n1 = new Node(1);
        Node n2 = new Node(2);
        Link l = new Link(n1, n2, 1);
        Demand d = new Demand();
        d.setAmount(3);
        d.setOrigin(n1);
        d.setTerminus(n2);
        Path p = new Path();
        p.addLink(l);
        d.setPaths(new ArrayList<>(Arrays.asList(p)));
        ODemand oDemand = new ODemand(d, 3);

        assertEquals("", p, oDemand.getSelectedPath());
        assertEquals("", 0, oDemand.getStartLambda());
    }

    @Test
    public void goToNextPathTest() {
        Node n1 = new Node(1);
        Node n2 = new Node(2);
        Link l = new Link(n1, n2, 1);
        Demand d = new Demand();
        d.setAmount(3);
        d.setOrigin(n1);
        d.setTerminus(n2);
        Path p = new Path();
        p.addLink(l);
        d.setPaths(new ArrayList<>(Arrays.asList(p)));
        ODemand oDemand = new ODemand(d, 3);

        assertEquals("", p, oDemand.getSelectedPath());
        assertEquals("", 0, oDemand.getStartLambda());
        oDemand.next();
        assertEquals("", p, oDemand.getSelectedPath());
        assertEquals("", 0, oDemand.getStartLambda());

        oDemand = new ODemand(d, 10);
        assertEquals("", p, oDemand.getSelectedPath());
        assertEquals("", 0, oDemand.getStartLambda());
        int counter = 0;
        while (oDemand.getStartLambda() == 0 && counter++ < 10) {
            oDemand.next();
            assertEquals("", p, oDemand.getSelectedPath());
        }

        if (counter >= 10 || oDemand.getStartLambda() == 0) {
            fail();
        }

    }
}
