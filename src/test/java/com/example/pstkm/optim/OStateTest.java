package com.example.pstkm.optim;

import com.example.pstkm.model.Demand;
import com.example.pstkm.model.Link;
import com.example.pstkm.model.Node;
import com.example.pstkm.model.Path;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by Mielek on 31.05.2016.
 */
public class OStateTest {
    @Test
    public void createObject() {
        List<Node> nodes = new ArrayList<>();
        List<Link> links = getLinks(nodes);
        List<Demand> demands = getDemands(links);
        for (Demand d : demands) {
            d.setAmount(1);
        }
        OState state = new OState(links, demands, 1);
        assertEquals("", 6, state.getStateCost(), 0.00001);
        assertEquals("", 6, state.dList.size());
        assertEquals("", 6, state.eList.size());
    }

    @Test
    public void nextStateTest() {
        List<Node> nodes = new ArrayList<>();
        List<Link> links = getLinks(nodes);
        List<Demand> demands = getDemands(links);
        for (Demand d : demands) {
            d.setAmount(1);
        }
        OState state = new OState(links, demands, 1);

        OState next = state.getNextState();

        assertNotEquals("", state.getStateCost(), next.getStateCost());
    }

    @Test
    public void nextStateTest2() {
        List<Node> nodes = new ArrayList<>();
        List<Link> links = getLinks(nodes);
        List<Demand> demands = getDemands(links);
        for (Demand d : demands) {
            d.setAmount(3);
            d.getPaths().remove(1);
        }
        OState state = new OState(links, demands, 5);
        OState next = state.getNextState();
        OState next2 = next.getNextState();
        next2.getNextState();
        next2.getNextState();
        next2.getNextState();
        next2.getNextState();
        next2.getNextState();
        next2.getNextState();
        next2.getNextState();
        next2.getNextState();
    }

    @Test
    public void nextStateTest3() {
        List<Node> nodes = new ArrayList<>();
        List<Link> links = getLinks(nodes);
        List<Demand> demands = getDemands(links);
        for (Demand d : demands) {
            d.setAmount(1);
            d.getPaths().remove(1);
        }
        OState state = new OState(links, demands, 1);
        assertEquals("", state.getStateCost(), state.getNextState().getStateCost(), 0.000001);
    }

    private List<Demand> getDemands(List<Link> links) {
        try {
            List<Demand> demands = new ArrayList<>();

            Demand d = new Demand();
            List<Path> paths = new ArrayList<>();
            Path p = new Path();
            p.addLink(links.get(0));
            paths.add(p);
            p = new Path();
            p.addLink(links.get(1));
            p.addLink(links.get(5));
            paths.add(p);
            d.setPaths(paths);
            d.setOrigin(p.getPathOrigin());
            d.setTerminus(p.getPathTerminus());
            demands.add(d);

            d = new Demand();
            paths = new ArrayList<>();
            p = new Path();
            p.addLink(links.get(1));
            paths.add(p);
            p = new Path();
            p.addLink(links.get(0));
            p.addLink(links.get(3));
            paths.add(p);
            d.setPaths(paths);
            d.setOrigin(p.getPathOrigin());
            d.setTerminus(p.getPathTerminus());
            demands.add(d);

            d = new Demand();
            paths = new ArrayList<>();
            p = new Path();
            p.addLink(links.get(2));
            paths.add(p);
            p = new Path();
            p.addLink(links.get(3));
            p.addLink(links.get(4));
            paths.add(p);
            d.setPaths(paths);
            d.setOrigin(p.getPathOrigin());
            d.setTerminus(p.getPathTerminus());
            demands.add(d);

            d = new Demand();
            paths = new ArrayList<>();
            p = new Path();
            p.addLink(links.get(3));
            paths.add(p);
            p = new Path();
            p.addLink(links.get(2));
            p.addLink(links.get(1));
            paths.add(p);
            d.setPaths(paths);
            d.setOrigin(p.getPathOrigin());
            d.setTerminus(p.getPathTerminus());
            demands.add(d);

            d = new Demand();
            paths = new ArrayList<>();
            p = new Path();
            p.addLink(links.get(4));
            paths.add(p);
            p = new Path();
            p.addLink(links.get(5));
            p.addLink(links.get(2));
            paths.add(p);
            d.setPaths(paths);
            d.setOrigin(p.getPathOrigin());
            d.setTerminus(p.getPathTerminus());
            demands.add(d);

            d = new Demand();
            paths = new ArrayList<>();
            p = new Path();
            p.addLink(links.get(5));
            paths.add(p);
            p = new Path();
            p.addLink(links.get(4));
            p.addLink(links.get(0));
            paths.add(p);
            d.setPaths(paths);
            d.setOrigin(p.getPathOrigin());
            d.setTerminus(p.getPathTerminus());
            demands.add(d);

            return demands;
        } catch (Exception e) {
            return null;
        }
    }

    private List<Link> getLinks(List<Node> nodes) {
        Node node = new Node(1);
        Node node2 = new Node(2);
        Node node3 = new Node(3);
        nodes.add(node);
        nodes.add(node2);
        nodes.add(node3);
        List<Link> links = new ArrayList<>();
        links.add(new Link(node, node2, 1));
        links.add(new Link(node, node3, 1));
        links.add(new Link(node2, node, 1));
        links.add(new Link(node2, node3, 1));
        links.add(new Link(node3, node, 1));
        links.add(new Link(node3, node2, 1));
        return links;
    }
}
