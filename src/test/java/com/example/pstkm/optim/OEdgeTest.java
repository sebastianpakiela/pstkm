package com.example.pstkm.optim;

import com.example.pstkm.model.Demand;
import com.example.pstkm.model.Link;
import com.example.pstkm.model.Node;
import com.example.pstkm.model.Path;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

/**
 * Created by Mielek on 31.05.2016.
 */
public class OEdgeTest {
    @Test
    public void edgeCreation() {
        Node n1 = new Node(1);
        Node n2 = new Node(2);
        Link l = new Link(n1, n2, 1);
        OEdge oEdge = new OEdge(l, 3);
        assertEquals("", 0, oEdge.getCurrentEdgeCost(), 0.000001);
        assertEquals("", 0, oEdge.getNbOfFibers());
    }

    @Test
    public void edgeCostTest() {
        Node n1 = new Node(1);
        Node n2 = new Node(2);
        Link l = new Link(n1, n2, 1);
        Demand d = new Demand();
        d.setAmount(3);
        d.setOrigin(n1);
        d.setTerminus(n2);
        Path p = new Path();
        p.addLink(l);
        d.setPaths(new ArrayList<>(Arrays.asList(p)));
        ODemand oDemand = new ODemand(d, 3);
        OEdge oEdge = new OEdge(l, 3);
        assertEquals("", 0, oEdge.getCurrentEdgeCost(), 0.000001);
        assertEquals("", 0, oEdge.getNbOfFibers());
        oEdge.addDemand(oDemand);
        assertEquals("", 1, oEdge.getCurrentEdgeCost(), 0.000001);
        assertEquals("", 1, oEdge.getNbOfFibers());
        d.setAmount(1);
        oDemand = new ODemand(d, 3);
        oEdge.addDemand(oDemand);
        assertEquals("", 2, oEdge.getCurrentEdgeCost(), 0.000001);
        assertEquals("", 2, oEdge.getNbOfFibers());
        oDemand.setStartLambda(1);
        oEdge.addDemand(oDemand);
        assertEquals("", 2, oEdge.getCurrentEdgeCost(), 0.000001);
        assertEquals("", 2, oEdge.getNbOfFibers());
        oDemand.setStartLambda(2);
        oEdge.addDemand(oDemand);
        assertEquals("", 2, oEdge.getCurrentEdgeCost(), 0.000001);
        assertEquals("", 2, oEdge.getNbOfFibers());

    }
}
